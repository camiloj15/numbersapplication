/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author camiloj15
 */
public class Digit {
    
    private int size;
    private int columns;
    private int rows;

    public Digit() {
    }

    /**
     *
     * @param size
     */
    public Digit(int size) {
        this.size = size;
        this.columns = size + 2;
        this.rows = (2*size)+3;
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }
        
    /**
     * El patron corresponde a todos los valores de la columna de aproximadamente
     * la mitad y diferentes a la fila media, la 0 y la ultima que contienen "|".
     * @return Arreglo de Strings con el dibujo del numero uno
     */
    public ArrayList<ArrayList<String>> numberOne(){
        ArrayList<ArrayList<String>> number = new ArrayList<>();
        ArrayList<String> column;
        int middle_row = (int) Math.floor(((double)rows)/2.0);
        int middle_column = (int) Math.floor(((double)columns)/2.0)-1;
        for (int i = 0; i < columns; i++) {
            column =  new ArrayList<>();
            for (int j = 0; j < rows; j++) {
                if(j!=0 && j!=(rows-1)&& j!=(middle_row)&&i==middle_column){
                    column.add("|");
                }else{
                    column.add(" ");
                }
            }
            number.add(column);
        }
        return number;
    }
    
    /**
     * El patron corresponde a todos los valores de la fila 0, la fila de la mitad
     * del total y la fila final contienen "-" excepto los de la ultima columna, 
     * la cual corresponde al espacio entre digitos; Por otro lado, los valores en
     * la columna 0 entre la fila del medio y la ultima contienen "|" al igual que
     * los de la penultima columna entre la primera fila y la de la mitad.
     * @return Arreglo de Strings con el dibujo del numero dos
     */
    public ArrayList<ArrayList<String>> numberTwo(){
        ArrayList<ArrayList<String>> number = new ArrayList<>();
        ArrayList<String> column;
        int middle_row = (int) Math.floor(((double)rows)/2.0);
        for (int i = 0; i < columns; i++) {
            column =  new ArrayList<>();
            for (int j = 0; j < rows; j++) {
                if( (j==0||j==(rows-1)||j==middle_row) && i!=(columns-1) && i!=0) {
                    column.add("-");
                }else if( (i==0 && j>middle_row && j!=(rows-1))) {
                    column.add("|");
                }else if( (i==(columns-2) && j<middle_row && j!=0)) {
                    column.add("|");
                }else{
                    column.add(" ");
                }
            }
            number.add(column);
        }
        return number;
    }
    
        /**
     * El patron corresponde a todos los valores de la fila 0, la fila de la mitad
     * del total y la fila final contienen "-" excepto los de la ultima columna, 
     * la cual corresponde al espacio entre digitos; Por otro lado, los valores en
     * la penultima columna entre la fila del medio y la ultima contienen "|" 
     * al igual que entre la primera fila y la de la mitad.
     * @return Arreglo de Strings con el dibujo del numero tres
     */
    public ArrayList<ArrayList<String>> numberThree(){
        ArrayList<ArrayList<String>> number = new ArrayList<>();
        ArrayList<String> column;
        int middle_row = (int) Math.floor(((double)rows)/2.0);
        for (int i = 0; i < columns; i++) {
            column =  new ArrayList<>();
            for (int j = 0; j < rows; j++) {
                if( (j==0||j==(rows-1)||j==middle_row) && i<(columns-2) ) {
                    column.add("-");
                }else if( (i==(columns-2) && j!=middle_row && j!=(rows-1) && j!=0)) {
                    column.add("|");
                }else{
                    column.add(" ");
                }
            }
            number.add(column);
        }
        return number;
    }
    
       /**
     * El patron corresponde a todos los valores la fila de la mitad que contienen "-" 
     * excepto los de la ultima columna,la cual corresponde al espacio entre digitos;
     * Por otro lado, los valores en la primera y penultima columna entre la 
     * fila del medio y la ultima contienen "|" al igual que en la penultima columna
     * entre la primera fila y la de la mitad.
     * @return Arreglo de Strings con el dibujo del numero dos
     */
    public ArrayList<ArrayList<String>> numberFour(){
        ArrayList<ArrayList<String>> number = new ArrayList<>();
        ArrayList<String> column;
        int middle_row = (int) Math.floor(((double)rows)/2.0);
        for (int i = 0; i < columns; i++) {
            column =  new ArrayList<>();
            for (int j = 0; j < rows; j++) {
                if( (j==middle_row) && i!=(columns-1) && i!=0 && i!=(columns-1)) {
                    column.add("-");
                }else if( (i==(columns-2) && j!=middle_row && j!=(columns-1)) && (j!=0&&j!=(rows-1))) {
                    column.add("|");
                }else if ( (i==0 && j<middle_row && j!=0) ) {
                    column.add("|");
                }else{
                    column.add(" ");
                }
            }
            number.add(column);
        }
        return number;
    }
    
     /**
     * El patron corresponde a todos los valores de la fila 0, la fila de la mitad
     * del total y la fila final contienen "-" excepto los de la ultima columna, 
     * la cual corresponde al espacio entre digitos; Por otro lado, los valores en
     * la penultima columna entre la fila del medio y la ultima contienen "|" 
     * al igual que los de la columna 0 entre la primera fila y la de la mitad.
     * @return Arreglo de Strings con el dibujo del numero dos
     */
    public ArrayList<ArrayList<String>> numberFive(){
        ArrayList<ArrayList<String>> number = new ArrayList<>();
        ArrayList<String> column;
        int middle_row = (int) Math.floor(((double)rows)/2.0);
        for (int i = 0; i < columns; i++) {
            column =  new ArrayList<>();
            for (int j = 0; j < rows; j++) {
                if( (j==0||j==(rows-1)||j==middle_row) && i!=(columns-1) && i!=0) {
                    column.add("-");
                }else if( (i==(columns-2) && j>middle_row && j!=(rows-1))) {
                    column.add("|");
                }else if( (i==0 && j<middle_row && j!=0)) {
                    column.add("|");
                }else{
                    column.add(" ");
                }
            }
            number.add(column);
        }
        return number;
    }
    
    /**
     * El patron corresponde a todos los valores de la fila 0, la fila de la mitad
     * del total y la fila final contienen "-" excepto los de la ultima columna, 
     * la cual corresponde al espacio entre digitos; Por otro lado, los valores en
     * la penultima columna y la columna entre la fila del medio y la ultima contienen "|" 
     * al igual que los de la columna 0 entre la primera fila y la de la mitad.
     * @return Arreglo de Strings con el dibujo del numero dos
     */
    public ArrayList<ArrayList<String>> numberSix(){
        ArrayList<ArrayList<String>> number = new ArrayList<>();
        ArrayList<String> column;
        int middle_row = (int) Math.floor(((double)rows)/2.0);
        for (int i = 0; i < columns; i++) {
            column =  new ArrayList<>();
            for (int j = 0; j < rows; j++) {
                if( (j==0||j==(rows-1)||j==middle_row) && i!=(columns-1) && i!=0) {
                    column.add("-");
                }else if( (i==(columns-2) && j>middle_row && j!=(rows-1))) {
                    column.add("|");
                }else if( (i==0 && j!=middle_row && j!=0 && j!=(rows-1))) {
                    column.add("|");
                }else{
                    column.add(" ");
                }
            }
            number.add(column);
        }
        return number;
    }
    
       /**
     * El patron corresponde a todos los valores de la fila 0 que contienen "-"
     * excepto los de la ultima columna,la cual corresponde al espacio entre digitos; 
     * Por otro lado, los valores en la penultima columna entre la fila del medio
     * y la ultima contienen "|" al igual que entre la primera fila y la de la mitad.
     * @return Arreglo de Strings con el dibujo del numero tres
     */
    public ArrayList<ArrayList<String>> numberSeven(){
        ArrayList<ArrayList<String>> number = new ArrayList<>();
        ArrayList<String> column;
        int middle_row = (int) Math.floor(((double)rows)/2.0);
        for (int i = 0; i < columns; i++) {
            column =  new ArrayList<>();
            for (int j = 0; j < rows; j++) {
                if( (j==0) && i<(columns-1) ) {
                    column.add("-");
                }else if( (i==(columns-2) && j!=middle_row && j!=(rows-1) && j!=0)) {
                    column.add("|");
                }else{
                    column.add(" ");
                }
            }
            number.add(column);
        }
        return number;
    }
    
    /**
     * El patron corresponde a todos los valores de la fila 0, la fila de la mitad
     * del total y la fila final contienen "-" excepto los de la ultima columna, 
     * la cual corresponde al espacio entre digitos; Por otro lado, los valores en
     * la penultima columna y la columna 0 entre la fila del medio y la ultima 
     * contienen "|" al igual que entre la primera fila y la de la mitad.
     * @return Arreglo de Strings con el dibujo del numero dos
     */
    public ArrayList<ArrayList<String>> numberEight(){
        ArrayList<ArrayList<String>> number = new ArrayList<>();
        ArrayList<String> column;
        int middle_row = (int) Math.floor(((double)rows)/2.0);
        for (int i = 0; i < columns; i++) {
            column =  new ArrayList<>();
            for (int j = 0; j < rows; j++) {
                if( (j==0||j==(rows-1)||j==middle_row) && i!=(columns-1) && i!=0) {
                    column.add("-");
                }else if( (i==(columns-2) && (j>middle_row || j<middle_row) && j!=(rows-1))) {
                    column.add("|");
                }else if( (i==0 && j!=middle_row && j!=0 && j!=(rows-1))) {
                    column.add("|");
                }else{
                    column.add(" ");
                }
            }
            number.add(column);
        }
        return number;
    }
    
    /**
     * El patron corresponde a todos los valores de la fila 0, la fila de la mitad
     * del total y la fila final contienen "-" excepto los de la ultima columna, 
     * la cual corresponde al espacio entre digitos; Por otro lado, los valores en
     * la penultima columna entre la fila del medio y la ultima 
     * contienen "|" al igual que entre la primera fila y la de la mitad de la
     * columna 0 y la penultima.
     * @return Arreglo de Strings con el dibujo del numero dos
     */
    public ArrayList<ArrayList<String>> numberNine(){
        ArrayList<ArrayList<String>> number = new ArrayList<>();
        ArrayList<String> column;
        int middle_row = (int) Math.floor(((double)rows)/2.0);
        for (int i = 0; i < columns; i++) {
            column =  new ArrayList<>();
            for (int j = 0; j < rows; j++) {
                if( (j==0||j==(rows-1)||j==middle_row) && i!=(columns-1) && i!=0) {
                    column.add("-");
                }else if( (i==(columns-2) && (j>middle_row || j<middle_row) && j!=(rows-1))) {
                    column.add("|");
                }else if( (i==0 && j<middle_row && j!=0)) {
                    column.add("|");
                }else{
                    column.add(" ");
                }
            }
            number.add(column);
        }
        return number;
    }
    
    /**
     * El patron corresponde a todos los valores de la fila 0, la fila de la mitad
     * del total y la fila final contienen "-" excepto los de la ultima columna, 
     * la cual corresponde al espacio entre digitos; Por otro lado, los valores en
     * la penultima columna y la columna 0 entre la fila del medio y la ultima 
     * contienen "|" al igual que entre la primera fila y la de la mitad.
     * @return Arreglo de Strings con el dibujo del numero dos
     */
    public ArrayList<ArrayList<String>> numberZero(){
        ArrayList<ArrayList<String>> number = new ArrayList<>();
        ArrayList<String> column;
        int middle_row = (int) Math.floor(((double)rows)/2.0);
        for (int i = 0; i < columns; i++) {
            column =  new ArrayList<>();
            for (int j = 0; j < rows; j++) {
                if( (j==0||j==(rows-1)) && i!=(columns-1) && i!=0) {
                    column.add("-");
                }else if( (i==(columns-2) && (j>middle_row || j<middle_row) && j!=(rows-1))) {
                    column.add("|");
                }else if( (i==0 && j!=middle_row && j!=0 && j!=(rows-1))) {
                    column.add("|");
                }else{
                    column.add(" ");
                }
            }
            number.add(column);
        }
        return number;
    }
}
