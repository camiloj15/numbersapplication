/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
/**
 *
 * @author camiloj15
 */
public class LedNumber {
    
    private int digit_size;
    private int[] digits;
    private String entry;
    
    private final static String MSG_SYNTAX_ERROR = "Error de sintaxis en la entrada.\n";
    private final static String REMENBER_INTRODUCTION_MESSAGE = ""
            + "Recuerda que cada línea contiene 2 números separados por coma."
            + "\nEl primer número representa el tamaño de la impresión"
            + "\n(entre 1 y 10 – esta variable se llama “size”). "
            + "\nEl segundo número es el número a mostrar en la pantalla."
            + "\nPara terminar, se debe digitar 0,0. Esto terminará la aplicación\n"
            + "Ejemplo: 2, 12345  || 3, 678\n";

    public LedNumber(String entry) {
        this.entry = entry;
    }
        
    /**
     * Metodo que genera el numero tipo led y lo imprime, en caso de error en el
     * validador de la entrada, este captura la excepcion y retorna el mensaje de
     * error para luego continuar con el ciclo normal.
     */
    public void generateLedNumber(){
        try{
        validateEntry(entry);
        }catch(Exception e){
           System.out.println(MSG_SYNTAX_ERROR); 
           System.out.println(REMENBER_INTRODUCTION_MESSAGE); 
           return;
        }
        Digit digitOne= new Digit(digit_size);
        ArrayList<ArrayList<String>> ledNumber = new ArrayList<>();
        for (int digit : digits) {
            switch(digit){
                case 1:
                    ledNumber.addAll(digitOne.numberOne());
                    break;
                case 2:
                    ledNumber.addAll(digitOne.numberTwo());
                    break;
                case 3:
                    ledNumber.addAll(digitOne.numberThree());
                    break;
                case 4:
                    ledNumber.addAll(digitOne.numberFour());
                    break;
                case 5:
                    ledNumber.addAll(digitOne.numberFive());
                    break;
                case 6:
                    ledNumber.addAll(digitOne.numberSix());
                    break;
                case 7:
                    ledNumber.addAll(digitOne.numberSeven());
                    break;
                case 8:
                    ledNumber.addAll(digitOne.numberEight());
                    break;
                case 9:
                    ledNumber.addAll(digitOne.numberNine());
                    break;
                case 0:
                    ledNumber.addAll(digitOne.numberZero());
                    break;
            }
        }
        for (int i = 0; i < ledNumber.get(0).size(); i++) {
            for (int j = 0; j < ledNumber.size(); j++) {
                       System.out.print(ledNumber.get(j).get(i)); 
            }
            System.out.println();
        }
    }
    
    /**
     * Metodo que valida que la entrada cumpla con la sintaxis, en caso contrario
     * dispara una excepcion.
     */
    public void validateEntry(String entry) throws Exception{
        String[] parts = entry.split(",");
        String[] numbers = parts[1].substring(1).split("");
        digit_size = Integer.valueOf(parts[0]);
        digits = new int[numbers.length];
        if(!parts[1].substring(0, 1).equalsIgnoreCase(" ")){
            throw new Exception();
        }
        for (int i = 0; i < numbers.length; i++) {
            digits[i] = Integer.valueOf(numbers[i]);
        }
        if(digit_size <1 || digit_size>11){
            throw new Exception();
        }
    }
}
