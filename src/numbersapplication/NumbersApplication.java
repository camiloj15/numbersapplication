/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numbersapplication;

import Model.Digit;
import Model.LedNumber;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author camiloj15
 */
public class NumbersApplication {

    private final static String MSG_INSERT_ENTRY = "Ingresa una nueva Entrada: ";
    private final static String EXIT_CHARACTER = "0,0";
    private final static String ENTER_ENTRY_MESSAGE = "Ingresa una nueva Entrada:";
    private final static String INTRODUCTION_MESSAGE = ""
            + "La entrada contiene varias líneas. "
            + "\nCada línea contiene 2 números separados por coma."
            + "\nEl primer número representa el tamaño de la impresión"
            + "\n(entre 1 y 10 – esta variable se llama “size”). "
            + "\nEl segundo número es el número a mostrar en la pantalla."
            + "\nPara terminar, se debe digitar 0,0. Esto terminará la aplicación\n"
            + "Ejemplo: 2, 12345  || 3, 678\n";
    /**
     * Clase principal que ejecuta el generador de numeros en un ciclo hasta
     * que el usuario salga de el
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         Scanner reader = new Scanner(System.in);  // Reading from System.in
         String entry;
         System.out.println(INTRODUCTION_MESSAGE);
         do{
         System.out.println(ENTER_ENTRY_MESSAGE);
         entry = reader.nextLine();
         LedNumber number = new LedNumber(entry);
         if (!entry.equalsIgnoreCase(EXIT_CHARACTER)){
         number.generateLedNumber();
         }
         }while(!entry.equalsIgnoreCase(EXIT_CHARACTER));
	}
    }
    

